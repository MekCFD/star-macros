// STAR-CCM+ macro: CreateTurbKineticEnergyFunctions.java
// Written by STAR-CCM+ 15.02.007
package macro;

import java.util.*;

import star.common.*;
import star.base.neo.*;
import star.base.report.*;


public class RecordReynoldsStresses extends StarMacro {

  public void execute() {

    // Create Velocity^2 field functions
    createVel2FieldFunction("Velocity ii", "vel_ii", "[$${Velocity}[0] * $${Velocity}[0], $${Velocity}[1] * $${Velocity}[1], $${Velocity}[2] * $${Velocity}[2]]");
    createVel2FieldFunction("Velocity ij", "vel_ij", "[$${Velocity}[0] * $${Velocity}[1], $${Velocity}[0] * $${Velocity}[2], $${Velocity}[1] * $${Velocity}[2]]");

    // Create Velocity^2 monitors
    for(int i = 0; i < 3; i++){  
        createMeanFlowMonitor("Velocity", i);
        createMeanFlowMonitor("vel_ii", i);
        createMeanFlowMonitor("vel_ij", i);
      }

    // Create Reynolds Stress field functions
    createVel2FieldFunction("Reynolds Stress ii", "rs_ii", "[${Meanvel_ii_0Monitor}-${MeanVelocity_0Monitor}*${MeanVelocity_0Monitor},${Meanvel_ii_1Monitor}-${MeanVelocity_1Monitor}*${MeanVelocity_1Monitor},${Meanvel_ii_2Monitor}-${MeanVelocity_2Monitor}*${MeanVelocity_2Monitor}]");
    createVel2FieldFunction("Reynolds Stress ij", "rs_ij", "[${Meanvel_ij_0Monitor}-${MeanVelocity_0Monitor}*${MeanVelocity_1Monitor},${Meanvel_ii_1Monitor}-${MeanVelocity_0Monitor}*${MeanVelocity_2Monitor},${Meanvel_ii_2Monitor}-${MeanVelocity_1Monitor}*${MeanVelocity_2Monitor}]");

    // Create Reynolds Stress monitors
    for(int i = 0; i < 3; i++){  
        createMeanFlowMonitor("rs_ii", i);
        createMeanFlowMonitor("rs_ij", i);
      }
    
  }

  private void createVel2FieldFunction(String name, String identifier, String definition) {

    Simulation simulation_0 = 
      getActiveSimulation();

    UserFieldFunction userFieldFunction_0 = 
      simulation_0.getFieldFunctionManager().createFieldFunction();

    userFieldFunction_0.getTypeOption().setSelected(FieldFunctionTypeOption.Type.VECTOR);

    userFieldFunction_0.setPresentationName(name);

    userFieldFunction_0.setFunctionName(identifier);

    userFieldFunction_0.setDimensions(Dimensions.Builder().length(2).time(-2).build());

    userFieldFunction_0.setDefinition(definition);
  }

  private void createMeanFlowMonitor(String vel, int component) {

    Simulation simulation_0 = 
      getActiveSimulation();

    FieldMeanMonitor fieldMeanMonitor_1 = 
      simulation_0.getMonitorManager().createMonitor(FieldMeanMonitor.class);

    fieldMeanMonitor_1.getParts().setQuery(null);

    Region region_0 = 
      simulation_0.getRegionManager().getRegion("Fluid");

    fieldMeanMonitor_1.getParts().setObjects(region_0);

    
    FieldFunction targetFieldFunction = 
      ((FieldFunction) simulation_0.getFieldFunctionManager().getFunction(vel));

    VectorComponentFieldFunction vectorComponentFieldFunction_0 = 
      ((VectorComponentFieldFunction) targetFieldFunction.getComponentFunction(component));

    fieldMeanMonitor_1.setFieldFunction(vectorComponentFieldFunction_0);

    fieldMeanMonitor_1.setPresentationName("Mean " + vel + "_" + component);
  }

}
